package com.example.godof.seat;

public class MemberDAO {
    String mem_id;
    String mem_gender;
    String mem_age;
    String mem_job;
    String mem_weight;
    String mem_body;

    public MemberDAO(String mem_id, String mem_gender, String mem_age, String mem_job, String mem_weight, String mem_body) {
        this.mem_id = mem_id;
        this.mem_gender = mem_gender;
        this.mem_age = mem_age;
        this.mem_job = mem_job;
        this.mem_weight = mem_weight;
        this.mem_body = mem_body;
    }

    public String getMem_id() {
        return mem_id;
    }

    public void setMem_id(String mem_id) {
        this.mem_id = mem_id;
    }

    public String getMem_gender() {
        return mem_gender;
    }

    public void setMem_gender(String mem_gender) {
        this.mem_gender = mem_gender;
    }

    public String getMem_age() {
        return mem_age;
    }

    public void setMem_age(String mem_age) {
        this.mem_age = mem_age;
    }

    public String getMem_job() {
        return mem_job;
    }

    public void setMem_job(String mem_job) {
        this.mem_job = mem_job;
    }

    public String getMem_weight() {
        return mem_weight;
    }

    public void setMem_weight(String mem_weight) {
        this.mem_weight = mem_weight;
    }

    public String getMem_body() {
        return mem_body;
    }

    public void setMem_body(String mem_body) {
        this.mem_body = mem_body;
    }
}
