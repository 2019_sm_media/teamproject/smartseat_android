package com.example.godof.seat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TestView extends AppCompatActivity {

    SharedPreferences setting;
    SharedPreferences.Editor editor;

    TextView tv_test;
    Button btn_alarm, btn_monitor, btn_data, btn_reset, btn_info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_view);

        init();

        setting = this.getSharedPreferences("pref", Context.MODE_PRIVATE);
        editor = setting.edit();
        tv_test = findViewById(R.id.tv_test);
        tv_test.setText(setting.getString("mem_id","")+" 님 안녕하세요.");

        // 정보초기화
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("Auto_Login_enabled", false);
                editor.putString("mem_id","");
                editor.commit();

                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                finish();
            }
        });



    }

    private void init() {

        tv_test = findViewById(R.id.tv_test);
        btn_data = findViewById(R.id.btn_data);
        btn_alarm = findViewById(R.id.btn_alarm);
        btn_monitor = findViewById(R.id.btn_monitor);
        btn_reset = findViewById(R.id.btn_reset);
        btn_info = findViewById(R.id.btn_info);
    }
}
