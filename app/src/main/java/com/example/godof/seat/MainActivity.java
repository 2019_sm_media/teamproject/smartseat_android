package com.example.godof.seat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    EditText edt_id,edt_age,edt_job,edt_weight,edt_body;
    Button btn_sign;
    TextView tv_title;
    RadioGroup rg_jender;

    SharedPreferences setting;
    SharedPreferences.Editor editor;

    final String PhoneNumber = "01066714320";
    final String BASE_URL = "http://172.30.1.1:8000";    // http:// 꼭 붙이자...ㅎㅎ

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        //자동 로그인 처리
        setting = this.getSharedPreferences("pref",Context.MODE_PRIVATE);
        editor = setting.edit();
        if(setting.getBoolean("Auto_Login_enabled",false)){
            Intent intent = new Intent(getApplicationContext(), TestView.class);
            Log.d("Auto_Login_enabled", setting.getBoolean("Auto_Login_enabled",false)+"");
            Log.d("Auto_Login_enabled_id", setting.getString("id", "0"));
            intent.putExtra("mem_id",setting.getString("id", "0"));
            startActivity(intent);
            finish();
        }


        //휴대폰번호 인증 & 모바일DB에 저장
        btn_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int id = rg_jender.getCheckedRadioButtonId();
                RadioButton rb = findViewById(id);

                MemberDAO member = new MemberDAO(edt_id.getText().toString(), rb.getText().toString(),
                        edt_age.getText().toString(), edt_job.getText().toString(),edt_weight.getText().toString(),edt_body.getText().toString());

                // pref id 저장
                editor.putString("mem_id", member.mem_id);
                editor.putString("mem_gender", member.mem_gender);
                editor.putString("mem_age", member.mem_age);
                editor.putString("mem_job", member.mem_job);
                editor.putString("mem_weight", member.mem_weight);
                editor.putString("mem_body", member.mem_body);
                editor.putBoolean("Auto_Login_enabled",true);
                editor.commit();

                // url 설정
                //http://172.30.1.54:8000/apps/sign/휴대폰번호/성별/나이/직업/
                String url = String.format(BASE_URL+"/apps/sign/%s/%s/%s/%s/%s/%s", member.getMem_id(),member.getMem_gender(), member.getMem_age(),
                        member.getMem_job(),member.getMem_weight(),member.getMem_body());
                AsyncDownThread thread = new AsyncDownThread(url,Nullhandler);
                thread.start();

                Intent intent = new Intent(getApplicationContext(), TestView.class);
                startActivity(intent);
                finish();

            }
        });

    }

    private void init() {
        edt_id = findViewById(R.id.edt_id);
        edt_age = findViewById(R.id.edt_age);
        edt_job = findViewById(R.id.edt_job);
        edt_body = findViewById(R.id.edt_body);
        edt_weight = findViewById(R.id.edt_weight);

        btn_sign = findViewById(R.id.btn_sign);
        rg_jender = findViewById(R.id.rg_jender);

        tv_title = findViewById(R.id.tv_title);
    }

    Handler Nullhandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            Log.d("nullhandler", msg.toString());
        }
    };

}

