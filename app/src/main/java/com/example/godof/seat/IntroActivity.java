package com.example.godof.seat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class IntroActivity extends AppCompatActivity {

    Handler handler;
    SharedPreferences setting;
    SharedPreferences.Editor editor;

    String mem_id;
    Class NextActivity = MainActivity.class;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //타이틀바 제거
        requestWindowFeature(getWindow().FEATURE_NO_TITLE);
        setContentView(R.layout.activity_intro);

        //정보 포팅
        setting = this.getSharedPreferences("pref", Context.MODE_PRIVATE);
        editor = setting.edit();
        mem_id = setting.getString("id","");

        //3초 동안의 인트로 화면
        handler = new Handler();
        handler.postDelayed(rIntent,3000);

    }

    Runnable rIntent = new Runnable() {
        @Override
        public void run() {
            if( ! mem_id.equals(""))
                NextActivity = TestView.class;
            Intent Main = new Intent(getApplicationContext(),NextActivity);
            startActivity(Main);
            finish();

            //fade in으로 시작하여 fade out으로 인트로 화면이 꺼지게 애니매니션 효과 적용
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(rIntent);
    }
}
